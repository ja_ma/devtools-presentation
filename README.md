# Information

This presentation uses [reveal js](https://revealjs.com/) as a git submodule following the instructions from [viegelinsh](https://gist.github.com/viegelinsch/1885ad4c3f2c85b615b946b8aa5d6738)

# Instructions to view the presentation

1. Clone this repository

`git clone https://gitlab.com/ja_ma/devtools-presentation.git`

2. Change into the new directory

`cd devtools-presentation`

3. Update reveal.js submodule

`git submodule update --init --recursive`

4. Open `index.html`
